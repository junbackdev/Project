<?php
    
    namespace socialmedia;
    
    class Comment {
        
        private string $text;
        private User $user;

        function __construct(User $user, string $text)
        {
            $this->user = $user;
            $this->text = $text;
        } 

        public function getUser(): User
        {
            return $this->user;
        }

        public function getText(): string
        {
            return $this->text;
        }
    }
?>
