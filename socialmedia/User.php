<?php

    namespace socialmedia;

    use DateTime;
    use Symfony\Component\Validator\Validation;
    use Symfony\Component\Validator\Constraints\Positive;
    use Symfony\Component\Validator\Constraints\NotBlank;
    use Symfony\Component\Validator\Constraints\Email;
    use Symfony\Component\Validator\Constraints\Length;
    use Symfony\Component\Validator\Constraints\Regex;

    class User {
        private int $id;
        private string $name;
        private string $email;
        private string $password;
        private DateTime $creationDateTime;

        public function __construct($id, $name, $email, $password)
        {
            $this->id=($this->idIsValid($id)) ? $id : 1;
            $this->name=($this->nameIsValid($name)) ? $name : "Unknown";
            $this->email=($this->emailIsValid($email)) ? $email : "unknown@unknown.com";
            $this->password=($this->passwordIsValid($password)) ? $password : "12345678";
            $this->creationDateTime = new DateTime($datetime = "now");
        }
        
        public function getCreationDateTime(): DateTime
        {
            return $this->creationDateTime;
	    }

        public function idIsValid(int $id): bool
	    {
	        $validator = Validation::createValidator();
            $idValidator = $validator->validate($id, [
                new Positive(),
                new NotBlank(),
            ]);

            if (0 !== count($idValidator)) {
                foreach ($idValidator as $val) {
                    echo $val->getMessage().'<br>';
                } 
                return false;   
            }

            return true;
        }

        public function nameIsValid(string $name): bool
	    {
	        $validator = Validation::createValidator();
            $nameValidator = $validator->validate($name, [
                new NotBlank(),
            ]);

            if (0 !== count($nameValidator)) {
                foreach ($nameValidator as $val) {
                    echo $val->getMessage().'<br>';
                } 
                return false;   
            }

            return true;
        }

        public function emailIsValid(string $email): bool
	    {
	        $validator = Validation::createValidator();
            $emailValidator = $validator->validate($email, [
                new NotBlank(),
                new Email(),
            ]);

            if (0 !== count($emailValidator)) {
                foreach ($emailValidator as $val) {
                    echo $val->getMessage().'<br>';
                } 
                return false;   
            }

            return true;
        }

        public function passwordIsValid(string $password): bool
	    {
	        $validator = Validation::createValidator();
            $passwordValidator = $validator->validate($password, [
                new NotBlank(),
                new Length(["min" => 8]),
            ]);

            if (0 !== count($passwordValidator)) {
                foreach ($passwordValidator as $val) {
                    echo $val->getMessage().'<br>';
                } 
                return false;   
            }

            return true;
        }

        public function getId(): int
        {
            return $this->id;
        }
    }
?>
