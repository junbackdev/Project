<?
	require 'vendor/autoload.php';

	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Validation;
    use socialmedia\User;
    use socialmedia\Comment;
    use DateTime;

	$validator = Validation::createValidator();
   
	$violations = $validator->validate('Bernhard', [
    		new Length(['min' => 10]),
    		new NotBlank(),
		]);

	if (0 !== count($violations)) {
    	// there are errors, now you can show them
    	foreach ($violations as $violation) {
        	echo $violation->getMessage().'<br>';
    	}
    }
    
    $user = new User(-11, "Ivan", "ivan@ivanov.ru", "1234");
    $user1 = new User(2, "Ivan", "ivan", "12345678");
    $user2 = new User(3, 5, "ivan@ivanov.ru", "12345678");
    $user3 = new User(4, "Ivan", "ivan@ivanov.ru", "1234!#$%");
    $user4 = new User(5, "Vasya", "ivan@ivanov.ru", "12340000");
    $user5 = new User(6, "Ivan", "ivan@ivanov.ru", "12341111");

    $comment = new Comment($user, "First");
    $comment1 = new Comment($user1, "Second");
    $comment2 = new Comment($user2, "Third");
    $comment3 = new Comment($user3, "Fourth");
    $comment4 = new Comment($user4, "Fifth");
    $comment5 = new Comment($user5, "Sixth");

    $comments = array($comment, $comment1, $comment2, $comment3, $comment4, $comment5 );

    $datetime = DateTime::createFromFormat('m-d-Y', '10-16-2003');

    foreach ($comments as $val)
        if ($val->getUser()->getCreationDateTime() > $datetime)
        {
            echo $val->getText();
            echo "<br>";    
        }

?>
